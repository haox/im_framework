
#include "InfluenceMaximization/TIMInfluenceCalculator.hpp"
#include "InfluenceMaximization/Graph.hpp"
#include "InfluenceMaximization/GenerateGraphLabels.hpp"
#include "cxxopts.hpp"
#include <iostream>
#include <string>
#include <sstream>
#include <set>
using namespace std;

cxxopts::ParseResult
parse(int argc, char* argv[]) {
  cxxopts::Options options("Influence calculator", "Experiments and research.");
  options
    .allow_unrecognised_options()
    .add_options()
    ("graph", "Graph file name", cxxopts::value<string>())
    ("seed", "Seed file name", cxxopts::value<string>())
    ("percentage", "Percentage of Targets", 
      cxxopts::value<int>()->default_value("80"))
    ("labelMethod", "Labelling Strategy", cxxopts::value<int>()->default_value("0"))
    ("epsilon", "", cxxopts::value<int>()->default_value("2"))
    ("model", "Cascade Model", cxxopts::value<string>()->default_value("IC"));
  auto result = options.parse(argc, argv);
  return result;
}

void createLabelFileIfNotExists(string graphFile, float percentage, LabelSetting labelSetting) {
    string labelFileName = Graph::constructLabelFileName(graphFile, percentage, labelSetting);
    ifstream labelFile(labelFileName);
    if (!labelFile.good()) {
      Graph *graph = new Graph;
      graph->readGraph(graphFile, 1);
      GenerateGraphLabels(graph, percentage, labelSetting);
      delete graph;
    }    
}

int main(int argc, char* argv[])
{
  auto result = parse(argc, argv);
  string graphFile = result["graph"].as<string>();
  float percentageTargets = (float)result["percentage"].as<int>()/100;
  int labelMethod = result["labelMethod"].as<int>();
  string model = result["model"].as<string>();
  int epsilon = result["epsilon"].as<int>();
  string seedFile = result["seed"].as<string>();

  set<int> seedSet;
  ifstream myFile(seedFile);
  if(!myFile.good()) {
      throw invalid_argument( "Seed file does not exist: " + seedFile);
  }
  if(myFile.is_open()) {    
    int seed;
    while (myFile >> seed) {
      seedSet.insert(seed);
    }
    myFile.close();
  }

  Graph *graph = new Graph;
  LabelSetting labelSetting = static_cast<LabelSetting>(labelMethod);
  createLabelFileIfNotExists(graphFile, percentageTargets, labelSetting);
  graph->readGraph(graphFile, percentageTargets, labelSetting);

  TIMInfluenceCalculator calculator(graph, epsilon, model);
  pair<int, int> influence = calculator.findInfluence(seedSet);

  cout << "\nexpectedTargets: " << influence.first;
  cout << "\nexpectedNonTargets: " << influence.second;

  return 0;
}

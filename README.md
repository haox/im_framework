# RUN
`cd src`

`python3 init.py` compiles all algorithms.

`python3 main.py` run multi algorithms with multi parameter groups which are configed in `config.py`.

---

# CONFIG

## config.py

`config.py` is for users to setting the input folder, the content of output and algorithms

```
CONFIG = {
    'INPUT': {
        # path of dataset (absolute / relative path)
        'path': '../IMM/nethept',
        # the diffusion model uses IC or LT
        'model': 'lt',
        # number of selected nodes
        'seedsize': 10
    },
    'OUTPUT': {
        # path of output file (including filename)
        'filepath': '../output.txt',
        # '*' for all kinds of output in DEFAULT['contents']. 
        # Allow choosing parts of contents as output. E.g., ['arguments', 'seeds', 'time']
        'contents': '*'
    },
    'ALGORITHM': {
        # parameters for specific algorithm
        # available algorithm: 'IMM', 'SIMPATH', 'LDAG', 'SSA', 'DSSA', 'TIM+', 'CELF', 'CELF++'
        'IMM': [{
            # parameter group 1  
            # a double value for epsilon
            'epsilon': 0.1
        }, {
            # parameter group 2  
            'epsilon': 0.2
        }]
    }
}

```


## Dataset Format
    Three files are necessary inside path to dataset folder

### Format for attribute
    Location:
        path_to_dataset/attribute.txt
    Format:
        This file should have exactly two lines
        n=number of nodes
        m=number of edges

### Format for IC model
    Location:
        path_to_dataset/graph_ic.inf
    Format:
        Each line has three numbers
        node1 node2 propogation_probability_from_node1_to_node2
    Comments:
        It is always a directed graph
        node number should range in [0 to n-1] (inclusive)

### Format for LT model
    Location:
        path_to_dataset/graph_lt.inf
    Format is same as IC model
    
---

# INTEGRATE ALGORITHM

This frame allows to integrate new IM algorithm by add a class implementing interface `IAlgorithm` in `src/algorithms`. The class name should be the same as file name.

We denote the standard graph input of our framework is inp_F. 

For each algorithm x, it should provide:

+ algorithm id
+ parameter list: framework will check all parameter are set in config.py
+ folder path: framework will initialize the algorithm by execuate `make` in the folder
+ Translate inp_F to inp_x
+ Translate param_F (let param_F be common parameter: model, seedsize) and param_x to command_line_x
+ extract common output (seedsize, time, memory) from output_x


---

# REFERENCE


The code for both CELF and CELF++ is present in the folder: `WWW11_CELF++`

The code for EaSyIM is present in the folder: `EaSyIM`

The code for IMM is present in the folder: `IMM`

The code for TIM+ is present in the folder: `TIM+_release_v1.1`

The code for StaticGreedy is present in the folder: `staticgreedycode`

The code for both LDAG and SIMPATH is present in the folder: `icdm11-simpath-release`

The code for both SSA and DSSA is present in the folder: `Stop-and-Stare-master`





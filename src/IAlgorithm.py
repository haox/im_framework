from getAbsPath import getAbsPath

class IAlgorithm(object):
    # @id: match the algorithm key in config
    # @folderPath: relative path from code repo root
    # @params: check all params are set in the begining
    def __init__(self, id, folderPath, params=[], influenceParams=[]):
        self.id = id
        self.params = params
        self.influenceParams = influenceParams
        self.folderPath = getAbsPath(folderPath)

    # Some algorithm may need specific format of input. This algorithm is for generating input file.
    # No return.
    # @inputArg: configs of input, including:
    #   model (lt / ic),
    #   seedsize,
    #   path (folder),
    #   filepath (.inf file path),
    #   dataset (folder name),
    #   attrPath (attribute.txt file path)
    #   tmpFolder (temporary folder for all generated input files)
    def getInputFile(self, inputArg):
        pass

    # This function is for extract standard results from console output
    # return: string, command line to execute algorithm
    # @inputArg: configs of input
    # @algArgObj: specific configs for this algorithm
    # @title: the identify shows the algArgObj is for which algorithm and parameter group
    def getCommandLine(self, inputArg, algArgObj, title):
        return ""

    # This function is for extract standard results from console output
    # return: dictionary, results
    # @output: the console log during exec command line
    # @outputContents: configs of output content
    def extractResult(self, output, outputContents):
        return {}

    # This function is for influence evaluation algorithms
    # return: string, influence value
    # @inputArg: configs of input
    # @seeds: string
    # @title: the identify shows the seeds comming from which algorithm and parameter group
    def getInfluence(self, inputArg, seeds, title):
        return ""

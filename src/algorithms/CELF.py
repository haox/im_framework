import os
import re

from IAlgorithm import IAlgorithm
from algorithms.SimPathUtil import getSeedsSimpath, getInputFileSimpath
from algorithms.CELFUtil import getCELFCommandline

class CELF(IAlgorithm):
    def __init__(self):
        super().__init__("CELF", 'WWW11_CELF++/celf++_code_release',
                         ['phase', 'mcruns'])  # id, folderPath, params
        self.tmpInput = ''

    def getInputFile(self, inputArg):
        filepathSimpath = os.path.join(inputArg['tmpFolder'], 'SIMPATH.inf')
        getInputFileSimpath(inputArg, filepathSimpath)
        self.tmpInput = filepathSimpath

    def getCommandLine(self, inputArg, algArgObj, title):
        return getCELFCommandline(self.id, inputArg, algArgObj, self.tmpInput, self.folderPath, title)

    def extractResult(self, output, outputContents):
        result = {}
        timePat = re.compile(r'total time taken : ([\d\.]+)\s*\n')
        memPat = re.compile(r'All memory released, current usage : ([\d\.]+)\s*\n')
        for c in outputContents:
            m = False
            if c == 'seeds':
                result[c] = getSeedsSimpath(output)
            elif c == 'time':
                time = timePat.findall(output)
                if time:
                    result[c] = time[0] + ' min'
            elif c == 'memory':
                m = memPat.findall(output)
            if m:
                result[c] = m[0]
        return result

import os


def getCELFCommandline(k, inputArg, algArgObj, filepathSimpath, CELFFolderPath, title):
    if k == 'CELF':
        celfPlus = '0'
    elif k == 'CELF++':
        celfPlus = '1'
    outputPathCELF = os.path.join(inputArg['tmpFolder'], 'output_' + title)
    if os.path.isdir(outputPathCELF):
        pass
    else:
        os.mkdir(outputPathCELF)

    # write config file for CELF
    configPath = os.path.abspath(os.path.join(inputArg['tmpFolder'], 'config_' + title + '.txt'))
    s = '''probGraphFile : {inputfile}
mcruns : {mcruns}
outdir : {outputCELF}
startIt : {startIt}
propModel : {model}
budget : {seedsize}
celfPlus : {celfPlus}
phase : {phase}
    '''
    s = s.format(inputfile=filepathSimpath,
                 outputCELF=outputPathCELF,
                 model=inputArg['model'].upper(),
                 seedsize=inputArg['seedsize'],
                 phase=algArgObj['phase'],
                 celfPlus=celfPlus,
                 mcruns=algArgObj['mcruns'],
                 startIt=algArgObj.get("startIt", 2))
    with open(configPath, 'wt') as fCELF:
        fCELF.write(s)

    return 'cd ' + CELFFolderPath + ' && ./InfluenceModels -c ' + configPath

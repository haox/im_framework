from IAlgorithm import IAlgorithm
from run import exeShell
import os
import re

class IM(IAlgorithm):
    def __init__(self):
        super().__init__("IM", 'InfluenceMaximization', []) # id, folderPath, params

    def getInputFile(self, inputArg):
        self.filepathIMGraph = os.path.join(inputArg['tmpFolder'], 'IMGraph.inf')
        if os.path.exists(self.filepathIMGraph):
            return
        nodePat = re.compile(r'n=(\d+)')
        edgePat = re.compile(r'm=(\d+)')
        nodeNum = 0
        edgeNum = 0
        with open(inputArg['attrPath'], 'rt') as f:
            for line in f:
                for n in nodePat.findall(line):
                    nodeNum = int(n)
                for n in edgePat.findall(line):
                    edgeNum = int(n)

        linePat = re.compile(r'(\d+)\s+(\d+)\s+(.+)$')

        with open(self.filepathIMGraph, 'wt') as fIM:
            fIM.write(str(nodeNum) + ' ' + str(edgeNum) + '\n')
            with open(inputArg['filepath'], 'rt') as f:
                for line in f:
                    m = linePat.match(line)
                    if m:
                        a = int(m.group(1))
                        b = int(m.group(2))
                        if a < b:
                            newLine = m.group(1) + ' ' + m.group(2) + '\n'
                            fIM.write(newLine)
                    else:
                        print(line)

    def getInfluence(self, inputArg, seed, title):
        # get seeds file
        tmpFolder = os.path.join(inputArg['tmpFolder'], 'seed')
        tmpSeedFile = os.path.join(tmpFolder, title + '.txt')
        if os.path.isdir(tmpFolder):
            pass
        else:
            os.mkdir(tmpFolder)
        if not os.path.exists(tmpSeedFile):
            with open(tmpSeedFile, 'wt') as fSeed:
                fSeed.write(seed)

        # run influence command
        output = exeShell('cd ' + self.folderPath + ' && ./cal --graph ' + self.filepathIMGraph + ' --seed '+ tmpSeedFile, tmpFolder, 'influence_' + title)

        #extractInfluenceResult
        expTarget = re.compile(r'expectedTargets:\s*([\d\.]+)\s*\n')
        expNonTarget = re.compile(r'expectedNonTargets:\s*([\d\.]+)')
        m = expTarget.findall(output)
        n = expNonTarget.findall(output)
        if m and n:
            return str(int(m[0])+int(n[0]))
        else:
            return ""


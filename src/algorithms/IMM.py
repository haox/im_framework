from IAlgorithm import IAlgorithm
import re

class IMM(IAlgorithm):
    def __init__(self):
        super().__init__("IMM", 'IMM', ['epsilon']) # id, folderPath, params

    # def getInputFile(self):
    #     pass

    def getCommandLine(self, inputArg, algArgObj, title):
        seedsizeStr = str(inputArg['seedsize'])
        modelUpper = inputArg['model'].upper()
        return 'cd ' + self.folderPath + ' && ./imm_discrete -dataset ' + inputArg['path'] + '/ -k ' + seedsizeStr + ' -model ' + modelUpper + ' -epsilon ' + str(
            algArgObj['epsilon'])

    def extractResult(self, output, outputContents):
        result = {}
        seedsPat = re.compile(r'g\.seedSet=([\d\s]+)\n')
        timePat = re.compile(r'Spend\s+([\d\.]+)\s+seconds\s+on\s+InfluenceMaximize')
        for c in outputContents:
            m = False
            if c == 'seeds':
                m = seedsPat.findall(output)
            elif c == 'time':
                m = timePat.findall(output)
            elif c == 'memory':
                # Didn't find memory in output
                pass
            if m:
                result[c] = m[0]
        return result

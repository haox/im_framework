import os
import re

from IAlgorithm import IAlgorithm
from algorithms.SimPathUtil import getSeedsSimpath, getInputFileSimpath, getSimpathCommandline

class LDAG(IAlgorithm):
    def __init__(self):
        super().__init__("LDAG", 'icdm11-simpath-release',
                         ['tol'])  # id, folderPath, params
        self.tmpInput = ''

    def getInputFile(self, inputArg):
        filepathSimpath = os.path.join(inputArg['tmpFolder'], 'SIMPATH.inf')
        getInputFileSimpath(inputArg, filepathSimpath)
        self.tmpInput = filepathSimpath

    def getCommandLine(self, inputArg, algArgObj, title):
        return getSimpathCommandline(self.id, inputArg, algArgObj, self.tmpInput, self.folderPath, title)

    def extractResult(self, output, outputContents):
        result = {}
        timePat = re.compile(r'total time taken before freeing alloacted memory : ([\d\.]+)\s*\n')
        memPat = re.compile(r'All memory released, current usage : ([\d\.]+)\s*\n')
        for c in outputContents:
            m = False
            if c == 'seeds':
                result[c] = getSeedsSimpath(output)
            elif c == 'time':
                m = timePat.findall(output)
            elif c == 'memory':
                m = memPat.findall(output)
            if m:
                result[c] = m[0]
        return result

import os
import re

from IAlgorithm import IAlgorithm
from algorithms.SSAUtil import getSeedsSSA, getInputFileSSA


class SSA(IAlgorithm):
    def __init__(self):
        super().__init__("SSA", 'Stop-and-Stare-master/SSA_release_2.0/DSSA',
                         ['epsilon', 'delta'])  # id, folderPath, params
        self.tmpBin = ''

    def getInputFile(self, inputArg):
        filepathSS = os.path.join(inputArg['tmpFolder'], 'SS.inf')
        filepathSSBin = os.path.join(inputArg['tmpFolder'], 'SS.bin')
        getInputFileSSA(inputArg, filepathSS, filepathSSBin, self.folderPath)
        self.tmpBin = filepathSSBin

    def getCommandLine(self, inputArg, algArgObj, title):
        seedsizeStr = str(inputArg['seedsize'])
        modelUpper = inputArg['model'].upper()
        return 'cd ' + self.folderPath + ' && ./DSSA -i ' + self.tmpBin + ' -k ' + seedsizeStr + ' -m ' + modelUpper + ' -epsilon ' + str(
            algArgObj['epsilon']) + ' -delta ' + str(
            algArgObj['delta'])

    def extractResult(self, output, outputContents):
        result = {}
        seedsPat = re.compile(r'Seed Nodes: ([\d\s]+\d+)\s+\n')
        timePat = re.compile(r'Time: ([\d\.]+s)')
        memPat = re.compile(r'Memory: ([\d\.]+\s*MB)')
        for c in outputContents:
            m = False
            if c == 'seeds':
                node = seedsPat.findall(output)
                if node:
                    result[c] = getSeedsSSA(node[0])
            elif c == 'time':
                m = timePat.findall(output)
            elif c == 'memory':
                m = memPat.findall(output)
            if m:
                result[c] = m[0]
        return result

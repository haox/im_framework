import os
from algorithms.SimPathUtil import getInputFileSimpath

# SSA, DSSA
def getSeedsSSA(node):
    node = node.split(" ")
    # all node id in SSA is added 1 when prepare input
    node = map(lambda x: str(int(x) - 1), node)
    return (" ").join(node)

# SSA, DSSA
def getInputFileSSA(inputArg, filepathSS, filepathSSBin, SSAFolderPath):
    if os.path.exists(filepathSSBin):
        return

    getInputFileSimpath(inputArg, filepathSS)

    # SSA/DSSA
    s = 'cd ' + SSAFolderPath + ' && ./el2bin ' + filepathSS + ' ' + filepathSSBin
    print(s)

    process = os.popen(s)
    output = process.read()
    process.close()
    print(output)

import os
import re


# SIMPATH, LDAG, CELF, CELF++
def getSeedsSimpath(output):
    node = []
    timePat = re.compile(r'Picked a seed node: (\d+),')
    m = timePat.findall(output)
    if m:
        # all node id in Simpath is added 1 when prepare input
        node = map(lambda x: str(int(x) - 1), m)
    return (" ").join(node)


# SIMPATH, LDAG, CELF, CELF++, SSA, DSSA
def getInputFileSimpath(inputArg, filepathSimpath):
    if os.path.exists(filepathSimpath):
        return
    nodePat = re.compile(r'n=(\d+)')
    edgePat = re.compile(r'm=(\d+)')
    nodeNum = 0
    edgeNum = 0
    with open(inputArg['attrPath'], 'rt') as f:
        for line in f:
            for n in nodePat.findall(line):
                nodeNum = int(n)
            for n in edgePat.findall(line):
                edgeNum = int(n)

    nodeNum2 = 0
    edgeNum2 = 0
    linePat = re.compile(r'(\d+)\s+(\d+)\s+(.+)$')

    with open(filepathSimpath, 'wt') as fSimpath:
        fSimpath.write(str(nodeNum) + ' ' + str(edgeNum) + '\n')
        with open(inputArg['filepath'], 'rt') as f:
            for line in f:
                m = linePat.match(line)
                if m:
                    edgeNum2 += 1
                    a = int(m.group(1)) + 1
                    b = int(m.group(2)) + 1
                    if a > nodeNum2:
                        nodeNum2 = a
                    if b > nodeNum2:
                        nodeNum2 = b
                    newLine = str(a) + ' ' + str(b) + ' ' + m.group(3) + '\n'
                    fSimpath.write(newLine)
                else:
                    print(line)
    # print(nodeNum2)
    # print(edgeNum2)
    if nodeNum2 != nodeNum or edgeNum2 != edgeNum:
        print('ALERT: The attribute.txt is incorrect! (attribute.txt, real count)')
        print('node: ' + str(nodeNum) + ' ' + str(nodeNum2))
        print('edge: ' + str(edgeNum) + ' ' + str(edgeNum2))


# SIMPATH, LDAG
def getSimpathCommandline(k, inputArg, algArgObj, filepathSimpath, SimpathFolderPath, title):
    if k == 'SIMPATH':
        phase = '17'
    elif k == 'LDAG':
        phase = '15'
    outputPathSimpath = os.path.join(inputArg['tmpFolder'], 'output_' + title)
    if os.path.isdir(outputPathSimpath):
        pass
    else:
        os.mkdir(outputPathSimpath)

    # write config file for SIMPATH
    configPath = os.path.join(inputArg['tmpFolder'], 'config_' + title + '.txt')
    s = '''probGraphFile : {filepathSimpath}

debug : 0
mcruns : 10000

outdir : {outputSimpath}

startIt : 2
propModel : {model}
cutoff : {cutoff}
topl : {topl}
budget : {seedsize}
celfPlus : 1
tol_ldag : {tol_ldag}


#phase
# 10 : MC
# 15 : LDAG
# 17 : Simple Path
# 20 : Baseline (Rand, HighDeg, PageRank, etc...)
phase : {phase}
    '''
    s = s.format(filepathSimpath=filepathSimpath,
                 outputSimpath=outputPathSimpath,
                 cutoff=algArgObj.get("cutoff", "0.001"),
                 topl=algArgObj.get("topl", "4"),
                 tol_ldag=algArgObj.get("tol", "0.003125"),
                 model=inputArg['model'].upper(),
                 seedsize=inputArg['seedsize'],
                 phase=phase)
    with open(configPath, 'wt') as fSimpath:
        fSimpath.write(s)

    return 'cd ' + SimpathFolderPath + ' && ./InfluenceModels -c ' + configPath

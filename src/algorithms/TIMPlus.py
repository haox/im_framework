from IAlgorithm import IAlgorithm
import re

class TIMPlus(IAlgorithm):
    def __init__(self):
        super().__init__("TIM+", 'TIM+_release_v1.1', ['epsilon']) # id, folderPath, params

    # def getInputFile(self):
    #     pass

    def getCommandLine(self, inputArg, algArgObj, title):
        seedsizeStr = str(inputArg['seedsize'])
        modelUpper = inputArg['model'].upper()
        return 'cd ' + self.folderPath + ' && ./tim -dataset ' + inputArg['path'] + '/ -k ' + seedsizeStr + ' -model ' + modelUpper + ' -epsilon ' + str(
            algArgObj['epsilon'])

    def extractResult(self, output, outputContents):
        result = {}
        seedsPat = re.compile(r'Selected k SeedSet: ([\d\s]+)\n')
        timePat = re.compile(r'Time used: ([\d\.]+s)')
        memPat = re.compile(r'Memory Usage: ([\d\.]+\s*MB)')
        for c in outputContents:
            m = False
            if c == 'seeds':
                m = seedsPat.findall(output)
            elif c == 'time':
                m = timePat.findall(output)
            elif c == 'memory':
                m = memPat.findall(output)
            if m:
                result[c] = m[0]
        return result

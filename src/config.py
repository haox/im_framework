CONFIG = {
    'INPUT': {
        # path of dataset
        'path': './dataset/com-amazon_graph',
        # the diffusion model uses IC or LT
        'model': 'lt',
        # number of selected nodes
        'seedsize': 10
    },
    'OUTPUT': {
        # path of output file (including filename)
        'filepath': './output.txt',
        # '*' for all kinds of output in DEFAULT['contents'].
        # Allow choosing parts of content for output. E.g., ['arguments', 'seeds', 'time']
        'contents': '*'
    },
    'INFLUENCE': {
        'IM': {}
    },
    'ALGORITHM': {
        # available algorithm: 'IMM', 'SIMPATH', 'LDAG', 'SSA', 'DSSA', 'TIM+', 'CELF', 'CELF++'
        'IMM': [{
            # a double value for epsilon
            'epsilon': 0.15
        }],
        'TIM+': [{
            # a double value for epsilon
            'epsilon': 0.15
        }],
        'SSA': [{
            # a double value for epsilon
            'epsilon': 0.15,
            'delta': 0.0001,
        }],
        'DSSA': [{
            'epsilon': 0.15,
            'delta': 0.0001,
        }],
        'CELF': [{
            # Must be 10 for influence maximization under IC/LT model.
            'phase': 10,
            # Number of monte carlo simulations to be used. Usually 10000.
            'mcruns': 10000
        }],
        # 'CELF++': [{
        #     # Must be 10 for influence maximization under IC/LT model.
        #     'phase': 10,
        #     # Number of monte carlo simulations to be used. Usually 10000.
        #     'mcruns': 10000,
        #     # iteration number at which the CELF++ optimization is invoked. Recommended value is 2.
        #     'startIt': 2
        # }],
        'SIMPATH': [{
            # the cut-off threshold for the backtrack method in SimPath and SPS-CELF++ .
            'cutoff': 0.0001,
            # topl (for SimPath only) : the number of items specified for the Look Ahead Optimization in SimPath.
            'topl': 4
        }],
        'LDAG': [{
            # the parameter for controlling the size of LDAG. The default value is 0.003125 (1/320).
            'tol': 0.003125
        }],
    }
}

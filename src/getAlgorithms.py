from os.path import dirname, isfile, basename
import glob
from IAlgorithm import IAlgorithm

# list all class in algorithms folder (class name must equals to file name; class must extends IAlgorithm)
def getAlgorithms():
    results = []
    modules = glob.glob(dirname(__file__)+"/algorithms/*.py")
    files= [ basename(f)[:-3] for f in modules if isfile(f) and not f.endswith('__init__.py')]
    for name in files:
        # add package prefix to name, if required
        module = __import__("algorithms."+name)
        for member in dir(module):
            # do something with the member named ``member``
            # print(member)
            handler_class = getattr(module, member)

            if handler_class and hasattr(handler_class, member):
                handler_class = getattr(handler_class, member)
                if issubclass(handler_class, IAlgorithm):
                    results.append(handler_class)
    return results

# for algoClass in getAlgorithms():
#     print(algoClass().id)
# Compile all algorithms
import subprocess, sys
from getAlgorithms import getAlgorithms

for algoClass in getAlgorithms():
    subprocess.call('cd ' + algoClass().folderPath + ' && make', shell=True)


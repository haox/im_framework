# only work for undirected graph
import sys
import os

filePath = str(sys.argv[1])
print ("The input file is: ", filePath)
if not os.path.isfile(filePath):
    print ("File not found")
    exit()

# Get In(v) and nodeNum, edgeNum
inDict = {}
with open(filePath, 'rt') as f:
    header_line = next(f)
    nodes = header_line.split("\n")[0].split(" ")
    nodeNum = nodes[0]
    edgeNum = nodes[1]

    for line in f:
        nodes = line.split("\n")[0].split(" ")
        nodeA = nodes[0]
        nodeB = nodes[1]
        if nodeB not in inDict:
            inDict[nodeB] = []
        inDict[nodeB].append(nodeA)
        if nodeA not in inDict:
            inDict[nodeA] = []
        inDict[nodeA].append(nodeB)

# make output folder
fileName = filePath.split('/')[-1].split('.')[0]
folderName = ('/').join(filePath.split('/')[0: -1]) + '/'+fileName+'_graph'
if os.path.isdir(folderName):
    print ("Folder already exists")
    exit()
else:
    os.mkdir(folderName)

# write attribute file
with open(folderName + "/attribute.txt", 'wt') as f:
    f.write("n="+nodeNum+"\nm="+str(int(edgeNum)*2))

# write IC file
degreeSum = 0
with open(folderName + "/graph_ic.inf", 'wt') as fIC:
    with open(folderName + "/graph_lt.inf", 'wt') as fLT:
        for i in range(int(nodeNum)):
            iStr = str(i)
            if iStr in inDict:
                degreeSum += len(inDict[iStr])
                for k in inDict[iStr]:
                    weight = str(round(1 / len(inDict[k]), 6))
                    fIC.write(iStr + " " + k + " " + weight + "\n")
                    fLT.write(iStr + " " + k + " " + weight + "\n")

avg = degreeSum/int(nodeNum)
print ("Avg Degree: " + str(avg))
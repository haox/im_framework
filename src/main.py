from config import CONFIG
from run import run
from getAbsPath import getAbsPath
from getAlgorithms import getAlgorithms
import os
import time


algoClassList = getAlgorithms()

# check config
DEFAULT = {
    'contents': ['arguments', 'influence', 'seeds', 'time', 'memory'],
    'algorithm': [],
    'inputTmpFolder': '../tmp',
    'param': {},
    'influenceParam': {},
    'algoObj': {}
}
for algoClass in algoClassList:
    algoObj = algoClass()
    id = algoObj.id
    DEFAULT['algorithm'].append(id)
    DEFAULT['algoObj'][id] = algoObj
    DEFAULT['param'][id] = algoClass().params
    DEFAULT['influenceParam'][id] = algoClass().influenceParams

if 'INPUT' in CONFIG:
    if 'model' in CONFIG['INPUT']:
        CONFIG['INPUT']['model'] = CONFIG['INPUT']['model'].lower()
    else:
        raise SystemExit('need INPUT model')
    if 'path' in CONFIG['INPUT']:
        CONFIG['INPUT']['path'] = os.path.abspath(CONFIG['INPUT']['path'])
        CONFIG['INPUT']['filepath'] = os.path.join(CONFIG['INPUT']['path'],
                                                   'graph_' + CONFIG['INPUT']['model'] + '.inf')
        CONFIG['INPUT']['attrPath'] = os.path.join(CONFIG['INPUT']['path'], 'attribute.txt')
        if os.path.isfile(CONFIG['INPUT']['attrPath']):
            pass
        else:
            raise SystemExit('INPUT attribute.txt file not exist')
        if os.path.isfile(CONFIG['INPUT']['filepath']):
            CONFIG['INPUT']['dataset'] = CONFIG['INPUT']['path'].split('/')[-1]
            t = time.time()
            CONFIG['INPUT']['tmpFolder'] = getAbsPath('tmp_' + CONFIG['INPUT']['dataset'] + '_' + str(int(t)))
        else:
            raise SystemExit('INPUT file not exist')
    else:
        raise SystemExit('need INPUT filepath')
    if 'seedsize' in CONFIG['INPUT']:
        pass
    else:
        raise SystemExit('need INPUT seedsize')
else:
    raise SystemExit('need INPUT')
if 'OUTPUT' in CONFIG:
    if 'filepath' in CONFIG['OUTPUT']:
        pass
    else:
        raise SystemExit('need OUTPUT filepath')
    if 'contents' in CONFIG['OUTPUT']:
        if CONFIG['OUTPUT']['contents'] == "*":
            CONFIG['OUTPUT']['contents'] = DEFAULT['contents']
        else:
            for i, k in enumerate(CONFIG['OUTPUT']['contents']):
                CONFIG['OUTPUT']['contents'][i] = k.lower().replace(' ', '')
    else:
        raise SystemExit('need OUTPUT contents')
    CONFIG['OUTPUT']['tmpFolder'] = os.path.join(CONFIG['INPUT']['tmpFolder'], 'output')
else:
    raise SystemExit('need OUTPUT')
if 'INFLUENCE' in CONFIG:
    for k in CONFIG['INFLUENCE']:
        if k in DEFAULT['algorithm']:
            paramObj = CONFIG['INFLUENCE'][k]
            for p in DEFAULT['influenceParam'][k]:
                if p in paramObj:
                    pass
                else:
                    raise SystemExit('need ' + p + ' for ' + k)
        else:
            raise SystemExit('there is no ' + k + ' algorithm')
if 'ALGORITHM' in CONFIG:
    for k in CONFIG['ALGORITHM']:
        if k in DEFAULT['algorithm']:
            if isinstance(CONFIG['ALGORITHM'][k], list):
                for paramObj in CONFIG['ALGORITHM'][k]:
                    for p in DEFAULT['param'][k]:
                        if p in paramObj:
                            pass
                        else:
                            raise SystemExit('need ' + p + ' for ' + k)
            else:
                raise SystemExit('It must be a List of parameter Dictionaries for ' + k + ' algorithm')
        else:
            raise SystemExit('there is no ' + k + ' algorithm')

else:
    raise SystemExit('need ALGORITHM')


# generate input files
if os.path.isdir(CONFIG['INPUT']['tmpFolder']):
    pass
else:
    os.mkdir(CONFIG['INPUT']['tmpFolder'])
for k in CONFIG['ALGORITHM']:
    DEFAULT['algoObj'][k].getInputFile(CONFIG['INPUT'])
for k in CONFIG['INFLUENCE']:
    DEFAULT['algoObj'][k].getInputFile(CONFIG['INPUT'])

run(CONFIG, DEFAULT['algoObj'])

# run the algorithms and write output according to config
import os


def run(arg, algoObj):
    results = {}
    if os.path.isdir(arg['OUTPUT']['tmpFolder']):
        pass
    else:
        os.mkdir(arg['OUTPUT']['tmpFolder'])

    for k in arg['ALGORITHM']:
        for i, algArgObj in enumerate(arg['ALGORITHM'][k]):
            title = k + "_" + str(i)
            results[title] = {}

            s = algoObj[k].getCommandLine(arg['INPUT'], algArgObj, title)
            output = exeShell(s, arg['OUTPUT']['tmpFolder'], title)
            results[title] = algoObj[k].extractResult(output, arg['OUTPUT']['contents'])

            if "seeds" in results[title]:
                for kInf in arg['INFLUENCE']:
                    results[title]['influence'] = algoObj[kInf].getInfluence(arg['INPUT'], results[title]["seeds"], title)

            if 'arguments' in arg['OUTPUT']['contents']:
                results[title]["arguments"] = ""
                for argKey in algArgObj:
                    results[title]["arguments"] += argKey + ": " + str(algArgObj[argKey]) + "; "

    # write output
    with open(arg['OUTPUT']['filepath'], 'wt') as fW:
        fW.write('title\n')
        for c in arg['OUTPUT']['contents']:
            fW.write(c + '\n')
        for k in results:
            fW.write('\n')
            fW.write(k + '\n')
            for c in arg['OUTPUT']['contents']:
                fW.write(results[k].get(c, "-"))
                fW.write("\n")


def exeShell(s, outputTmpFolder, k):
    print(s)
    process = os.popen(s)
    output = process.read()
    process.close()
    print(output)
    # write output
    with open(os.path.join(outputTmpFolder, k + '.txt'), 'wt') as fW:
        fW.write(output)
    return output